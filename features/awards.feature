Feature: /awards command
  As a movie enthusiast, i want to be able to check the awards won by a movie.

  Scenario: user uses command with an award winning movie
    When The user sends "/awards Titanic" to the bot
    Then The bot responds with "Won 11 Oscars, 126 wins & 83 nominations total"

    Scenario: user uses command with a movie without awards or nominations
      When The user sends "/awards Emoji" to the bot
      Then The bot responds with "Has no awards"

    Scenario: user uses command with a movie with only nominations
      When The user sends "/awards Heat" to the bot
      Then The bot responds with "Has 15 nominations"

    Scenario: user uses command with a movie that does not exist
      When The user sends "/awards A movie that does not exist" to the bot
      Then The bot responds with "The movie does not exist"

    Scenario: user uses command with multiple movies
      When The user sends "/awards Heat; Titanic; Emoji; A movie that does not exist" to the bot
      Then The bot responds with "Has 15 nominations"
      Then The bot responds with "Won 11 Oscars, 126 wins & 83 nominations total"
      Then The bot responds with "Has no awards"
      Then The bot responds with "The movie does not exist"



