require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_request_movies(movie_title, expected_answer)
  api_omdb_response_body = {
    "Title": movie_title,
    "Awards": expected_answer
  }

  stub_request(:get, "https://www.omdbapi.com/?apikey&t=#{movie_title}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

def stub_request_movie_does_not_exist(movie_title)
  api_omdb_response_body = {
    "Response": false,
    "Error": 'Movie not found!'
  }

  stub_request(:get, "https://www.omdbapi.com/?apikey&t=#{movie_title}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

def stub_request_multiple_movies
  stub_request_movies('Titanic', 'Won 11 Oscars, 126wins & 83 nominations total')
  stub_request_movies('Emoji', 'Has no awards')
  stub_request_movies('Heat', '15 nominations')
  stub_request_movie_does_not_exist('A movie that does not exist')
end

def multiple_movies_send_get_texts
  when_i_send_text('fake_token', '/awards Titanic; Emoji; Heat; A movie that does not exist')

  then_i_get_text('fake_token', 'Won 11 Oscars, 126wins & 83 nominations total')

  then_i_get_text('fake_token', 'Has no awards')

  then_i_get_text('fake_token', '15 nominations')

  then_i_get_text('fake_token', 'The movie does not exist')
end

describe 'BotClient' do
  it 'should get a /version message and respond with current version' do
    token = 'fake_token'

    when_i_send_text(token, '/version')
    then_i_get_text(token, Version.current)

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    token = 'fake_token'

    when_i_send_text(token, '/say_hi Emilio')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    token = 'fake_token'

    when_i_send_text(token, '/start')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /stop message and respond with Chau' do
    token = 'fake_token'

    when_i_send_text(token, '/stop')
    then_i_get_text(token, 'Chau, egutter')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    token = 'fake_token'

    when_i_send_text(token, '/tv')
    then_i_get_keyboard_message(token, 'Quien se queda con el trono?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    token = 'fake_token'

    when_i_send_keyboard_updates(token, 'Quien se queda con el trono?', '2')
    then_i_get_text(token, 'A mi también me encantan los dragones!')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    token = 'fake_token'

    when_i_send_text(token, '/unknown')
    then_i_get_text(token, 'Uh? No te entiendo! Me repetis la pregunta?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an /awards message and respond with how many oscars won' do
    stub_request_movies('Titanic',  'Won 11 Oscars, 126wins & 83 nominations total')

    when_i_send_text('fake_token', '/awards Titanic')
    then_i_get_text('fake_token', 'Won 11 Oscars, 126wins & 83 nominations total')

    app = BotClient.new('fake_token')

    app.run_once
  end

  it 'using /awards with a movie without an award or nomination sends a Movie has no awards message' do
    stub_request_movies('Emoji', 'Has no awards')

    when_i_send_text('fake_token', '/awards Emoji')
    then_i_get_text('fake_token', 'Has no awards')

    app = BotClient.new('fake_token')

    app.run_once
  end

  it 'using /awards with a movie with only nominations sends a Movie has nominations message' do
    stub_request_movies('Heat', '15 nominations')

    when_i_send_text('fake_token', '/awards Heat')
    then_i_get_text('fake_token', '15 nominations')

    app = BotClient.new('fake_token')

    app.run_once
  end

  it 'using /awards with a movie that does not exist sends a Movie does not exist message' do
    stub_request_movie_does_not_exist('A movie that does not exist')

    when_i_send_text('fake_token', '/awards A movie that does not exist')
    then_i_get_text('fake_token', 'The movie does not exist')

    app = BotClient.new('fake_token')

    app.run_once
  end

  it 'using /awards with multiple movies returns the correct message for each movie' do
    stub_request_multiple_movies

    multiple_movies_send_get_texts

    app = BotClient.new('fake_token')

    app.run_once
  end
end
