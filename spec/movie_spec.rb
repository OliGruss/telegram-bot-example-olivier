describe 'Movie' do
  it 'should get the correct title' do
    movie = Movie.new('Titanic', 'Won 11 Oscars, 126 wins & 83 nominations total')
    expect(movie.title).to eq 'Titanic'
  end

  it 'should get the correct awards' do
    movie = Movie.new('Titanic', 'Won 11 Oscars, 126 wins & 83 nominations total')
    expect(movie.awards).to eq 'Won 11 Oscars, 126 wins & 83 nominations total'
  end

  it 'has_awards? should be false when the movie does not have any award or nomination' do
    movie = Movie.new('Titanic', 'N/A')
    expect(movie.has_awards?).to eq false
  end
end
