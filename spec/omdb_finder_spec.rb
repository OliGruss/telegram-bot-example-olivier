def stub_request_movie_not_exist
  api_omdb_response_body = {
    "Response": false,
    "Error": 'Movie not found!'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=A%20movie%20that%20does%20not%20exist')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body, headers: {})
end

describe 'OmdbFinder' do
  describe 'create_movie_with_awards' do
    it 'should return nil when it cant find the movie in the omdb database' do
      stub_request_movie_not_exist

      finder = OmdbFinder.new
      result = finder.create_movie_with_awards('A movie that does not exist')
      expect(result).to eq nil
    end
  end
end
