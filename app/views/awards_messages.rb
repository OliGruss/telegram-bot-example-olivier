class AwardsMessages
  def awards_message(movie)
    if movie.nil?
      'The movie does not exist'
    elsif movie.has_awards?
      movie.awards.to_s
    else
      'Has no awards'
    end
  end
end
