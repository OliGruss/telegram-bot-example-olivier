require 'faraday'
require_relative '../tv/movie'

class OmdbFinder
  API_URL = 'https://www.omdbapi.com/'.freeze
  API_KEY = ENV['OMDB_API_KEY']

  def create_movie_with_awards(movie_name)
    response = Faraday.get(API_URL, { t: movie_name, apikey: API_KEY })
    response_json = JSON.parse(response.body)

    if response_json['Error'] == 'Movie not found!'
      nil
    else
      Movie.new(response_json['Title'], response_json['Awards'])
    end
  end
end
